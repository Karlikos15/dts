﻿using System.ComponentModel.DataAnnotations.Schema;

namespace DTSWeb.Models
{
    public class Disk
    {
        public int DiskId { get; set; }
        public int WindingId { get; set; }
        [ForeignKey("WindingId")]
        public Winding Winding { get; set; }
    }
}