﻿using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace DTSWeb.Models
{
    public class ShieldingScreen
    {
        public int ShieldingScreenId { get; set; }
        public int WindingId { get; set; }
        [ForeignKey("WindingId")]
        public Winding Winding { get; set; }
    }
}