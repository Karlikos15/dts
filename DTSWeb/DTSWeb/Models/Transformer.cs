﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace DTSWeb.Models
{
    public class Transformer
    {
        public int TransformerId { get; set; }
        [DefaultValue(1)]
        public int CoolingTypeId { get; set; }
        [ForeignKey("CoolingTypeId")]
        public CoolingType CoolingType { get; set; }
        [DefaultValue(1)]
        public int PhaseId { get; set; }
        [ForeignKey("PhaseId")]
        public Phase Phase { get; set; }
        [DefaultValue("Transformer")]
        public string DesignId { get; set; }
        [DefaultValue(0)]
        public double KVa { get; set; }
        [DefaultValue(0)]
        public double Frequency { get; set; }
        [DefaultValue(false)]
        public bool Acr { get; set; }
        [DefaultValue(0)]
        public int Tamb { get; set; }
        [DefaultValue(0)]
        public int Tref { get; set; }
    }
}