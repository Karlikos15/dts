﻿namespace DTSWeb.Models
{
    public class LegShape
    {
        public int LegShapeId { get; set; }
        public string Name { get; set; }
    }
}