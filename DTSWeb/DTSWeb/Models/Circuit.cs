﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace DTSWeb.Models
{
    public class Circuit
    {
        public int CircuitId { get; set; }

        public int CircuitConnectionId { get; set; }
        [ForeignKey("CircuitConnectionId")]
        public CircuitConnection CircuitConnection { get; set; }

        public int SequenceId { get; set; }
        [ForeignKey("SequenceId")]
        public Sequence Sequence { get; set; }

        public int TransformerId { get; set; }
        [ForeignKey("TransformerId")]
        public Transformer Transformer { get; set; }

        public double KVa { get; set; }
        public double Vline { get; set; }
        public double MinTapping { get; set; }
        public double MaxTapping { get; set; }
        public double AnalyzeTapping { get; set; }
        public int TabPosition { get; set; }
    }
}