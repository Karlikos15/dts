﻿namespace DTSWeb.Models
{
    public class Technology
    {
        public int TechnologyId { get; set; }
        public string Name { get; set; }
    }
}