﻿using System.ComponentModel.DataAnnotations.Schema;

namespace DTSWeb.Models
{
    public class Lead
    {
        public int LeadId { get; set; }
        public int WindingId { get; set; }
        [ForeignKey("WindingId")]
        public Winding Winding { get; set; }
    }
}