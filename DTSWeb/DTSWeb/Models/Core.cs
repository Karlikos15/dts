﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace DTSWeb.Models
{
    public class Core
    {
        public int CoreId { get; set; }

        public int LegShapeId { get; set; }
        [ForeignKey("LegShapeId")]
        public LegShape LegShape { get; set; }

        public int TransformerId { get; set; }
        [ForeignKey("TransformerId")]
        public Transformer Transformer { get; set; }

        public double NetCsa { get; set; }
        public double GrossCsa { get; set; }
        public double MaxWidth { get; set; }
        public double Diam { get; set; }
        public double WindowWidth { get; set; }
        public double LegCenters { get; set; }
        public double VoltsPerTurn { get; set; }
        public double B { get; set; }
        public double Nll { get; set; }
        public int CoolingDucts { get; set; }
        public double Width { get; set; }

    }
}
