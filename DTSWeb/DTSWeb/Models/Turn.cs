﻿using System.ComponentModel.DataAnnotations.Schema;

namespace DTSWeb.Models
{
    public class Turn
    {
        public int TurnId { get; set; }
        public int WindingId { get; set; }
        [ForeignKey("WindingId")]
        public Winding Winding { get; set; }
    }
}