﻿namespace DTSWeb.Models
{
    public class CircuitConnection
    {
        public int CircuitConnectionId { get; set; }
        public string Name { get; set; }
    }
}