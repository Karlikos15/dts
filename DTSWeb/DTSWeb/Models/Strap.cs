﻿using System.ComponentModel.DataAnnotations.Schema;

namespace DTSWeb.Models
{
    public class Strap
    {
        public int StrapId { get; set; }
        public int TurnId { get; set; }
        [ForeignKey("TurnId")]
        public Turn Turn { get; set; }
    }
}