﻿using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace DTSWeb.Models
{
    public class Winding
    {  
        public int WindingId { get; set; }
        public int TechnologyId { get; set; }
        [ForeignKey("TechnologyId")]
        [JsonIgnore]
        public Technology Technology { get; set; }
        [JsonIgnore]
        public int? MechanicalGroupId { get; set; }
        [ForeignKey("MechanicalGroupId")]
        [JsonIgnore]
        public MechanicalGroup MechanicalGroup { get; set; }
        public int CircuitId { get; set; }
        [ForeignKey("CircuitId")]
        [JsonIgnore]
        public Circuit Circuit { get; set; }
        public string DesignId { get; set; }
        public double Tap { get; set; }
    }
}