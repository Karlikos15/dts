﻿using System.ComponentModel.DataAnnotations.Schema;

namespace DTSWeb.Models
{
    public class CoolingDuct
    {
        public int CoolingDuctId { get; set; }
        public int WindingId { get; set; }
        [ForeignKey("WindingId")]
        public Winding Winding { get; set; }
    }
}