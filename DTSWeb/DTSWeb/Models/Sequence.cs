﻿namespace DTSWeb.Models
{
    public class Sequence
    {
        public int SequenceId { get; set; }
        public string Name { get; set; }
    }
}