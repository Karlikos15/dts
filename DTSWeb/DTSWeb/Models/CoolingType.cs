﻿namespace DTSWeb.Models
{
    public class CoolingType
    {
        public int CoolingTypeId { get; set; }
        public string Name { get; set; }
    }
}