﻿namespace DTSWeb.Models
{
    public class Phase
    {
        public int PhaseId { get; set; }
        public int Value { get; set; }
    }
}