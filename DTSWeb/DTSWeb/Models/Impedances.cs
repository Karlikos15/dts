﻿namespace DTSWeb.Models
{
    public class Impedances
    {
        public double Feeders { get; set; }
        public double Shortcircuited { get; set; }
        public double XccWindings { get; set; }
        public double RccWindings { get; set; }
        public double XccBusbars { get; set; }
        public double RccBusbars { get; set; }
        public double Zcc { get; set; }
    }
}