﻿namespace DTSWeb.Models
{
    public class WindingConnection
    {
        public int WindingConnectionId { get; set; }
        public string Name { get; set; }
    }
}