﻿using System.ComponentModel.DataAnnotations.Schema;

namespace DTSWeb.Models
{
    public class Result
    {
        public int ResultId { get; set; }
        public int TransformerId { get; set; }
        [ForeignKey("TransformerId")]
        public Transformer Transformer { get; set; }
    }
}