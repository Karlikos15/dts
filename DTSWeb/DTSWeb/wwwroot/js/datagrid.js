﻿// Get technologies select 
var technologies;

var tabPosition;
var mechGroups = [
    { "ID": "1", "Top Insulation": 1, "Bottom Insulation": 1, "Inner Insulation": 1, "Material": 1,  "Windings Contained": "C2-D-LV1D,C2-Ext-LV1Y" },
    { "ID": "2", "Top Insulation": 1, "Bottom Insulation": 1, "Inner Insulation": 1, "Material": 1, "Windings Contained": "C2-D-LV1D,C2-Ext-LV1Y" },
    { "ID": "3", "Top Insulation": 1, "Bottom Insulation": 1, "Inner Insulation": 1, "Material": 1, "Windings Contained": "C2-D-LV1D,C2-Ext-LV1Y" },
    { "ID": "4", "Top Insulation": 1, "Bottom Insulation": 1, "Inner Insulation": 1, "Material": 1, "Windings Contained": "C2-D-LV1D,C2-Ext-LV1Y" },
    { "ID": "5", "Top Insulation": 1, "Bottom Insulation": 1, "Inner Insulation": 1, "Material": 1, "Windings Contained": "C2-D-LV1D,C2-Ext-LV1Y"},
    { "ID": "6", "Top Insulation": 1, "Bottom Insulation": 1, "Inner Insulation": 1, "Material": 1, "Windings Contained": "C2-D-LV1D,C2-Ext-LV1Y"},
    { "ID": "7", "Top Insulation": 1, "Bottom Insulation": 1, "Inner Insulation": 1, "Material": 1, "Windings Contained": "C2-D-LV1D,C2-Ext-LV1Y"},
    { "ID": "8", "Top Insulation": 1, "Bottom Insulation": 1, "Inner Insulation": 1, "Material": 1, "Windings Contained": "C2-D-LV1D,C2-Ext-LV1Y"},
    { "ID": "9", "Top Insulation": 1, "Bottom Insulation": 1, "Inner Insulation": 1, "Material": 1, "Windings Contained": "C2-D-LV1D,C2-Ext-LV1Y"},
    { "ID": "10", "Top Insulation": 1, "Bottom Insulation": 1, "Inner Insulation": 1, "Material": 1, "Windings Contained": "C2-D-LV1D,C2-Ext-LV1Y"},
    { "ID": "11", "Top Insulation": 1, "Bottom Insulation": 1, "Inner Insulation": 1, "Material": 1, "Windings Contained": "C2-D-LV1D,C2-Ext-LV1Y"},
    { "ID": "12", "Top Insulation": 1, "Bottom Insulation": 1, "Inner Insulation": 1, "Material": 1, "Windings Contained": "C2-D-LV1D,C2-Ext-LV1Y"},
    { "ID": "13", "Top Insulation": 1, "Bottom Insulation": 1, "Inner Insulation": 1, "Material": 1, "Windings Contained": "C2-D-LV1D,C2-Ext-LV1Y"},
    { "ID": "14", "Top Insulation": 1, "Bottom Insulation": 1, "Inner Insulation": 1, "Material": 1, "Windings Contained": "C2-D-LV1D,C2-Ext-LV1Y"},
    { "ID": "15", "Top Insulation": 1, "Bottom Insulation": 1, "Inner Insulation": 1, "Material": 1, "Windings Contained": "C2-D-LV1D,C2-Ext-LV1Y"},
    { "ID": "16", "Top Insulation": 1, "Bottom Insulation": 1, "Inner Insulation": 1, "Material": 1, "Windings Contained": "C2-D-LV1D,C2-Ext-LV1Y"},
    { "ID": "17", "Top Insulation": 1, "Bottom Insulation": 1, "Inner Insulation": 1, "Material": 1, "Windings Contained": "C2-D-LV1D,C2-Ext-LV1Y"},
    { "ID": "18", "Top Insulation": 1, "Bottom Insulation": 1, "Inner Insulation": 1, "Material": 1, "Windings Contained": "C2-D-LV1D,C2-Ext-LV1Y"},
    { "ID": "19", "Top Insulation": 1, "Bottom Insulation": 1, "Inner Insulation": 1, "Material": 1, "Windings Contained": "C2-D-LV1D,C2-Ext-LV1Y"},
    { "ID": "20", "Top Insulation": 1, "Bottom Insulation": 1, "Inner Insulation": 1, "Material": 1, "Windings Contained": "C2-D-LV1D,C2-Ext-LV1Y"},
    { "ID": "21", "Top Insulation": 1, "Bottom Insulation": 1, "Inner Insulation": 1, "Material": 1, "Windings Contained": "C2-D-LV1D,C2-Ext-LV1Y" }
];

var materials = [
    { Name: "Epoxy", Id: 0 },
    { Name: "Impregnated", Id: 1 },
    { Name: "OtherMechGroupFiller", Id: 2 }
];


function reloadWindings(tabId) {
    tabPosition = tabId;
    $(".windings-circuit-grid").jsGrid({
        width: "100%",

        inserting: true,
        editing: true,
        sorting: true,
        autoload: true,
        //paging: true,

        invalidMessage: "Invalid data entered",

        editButton: false,
        controller : {
            loadData: function (filter) {
                var def = $.Deferred();
                // Get windings of selected circuit
                $.ajax({
                    type: "GET",
                    dataType: "json",
                    url: '/windings/' + tabPosition.toString(),
                    success: function (data) {

                        // Return response
                        console.log(data);
                        def.resolve(data);
                    },
                    error: function (ts) {
                        //alert("Error");
                    }
                });
                return def.promise();
            },
            insertItem: function (item) {
                var def = $.Deferred();

                // Get windings of selected circuit
                $.ajax({
                    type: "POST",
                    dataType: "json",
                    data: { winding: item, tabPosition: tabPosition },
                    url: '/windings',
                    success: function (data) {
      
                        // Return response
                        console.log(data);
                        def.resolve(data);
                    },
                    error: function(ts) {
                        //alert(ts.responseText);
                        //alert(ts.responseStatus);
                    }
                });
                return def.promise();
            },
            updateItem: function (item) {
                var def = $.Deferred();
                // Get windings of selected circuit
                $.ajax({
                    type: "PUT",
                    dataType: "json",
                    data: { winding: item, tabPosition: tabPosition },
                    url: '/windings/' + item.windingId,
                    success: function (data) {

                        // Return response
                        console.log(data);
                        def.resolve(data);
                    },
                    error: function (ts) {
                        //alert(ts.responseText);
                        //alert(ts.responseStatus);
                    }
                });
                return def.promise();
            },
            deleteItem: function (item) {
                var def = $.Deferred();
                // Get windings of selected circuit
                $.ajax({
                    type: "DELETE",
                    url: '/windings/' + item.windingId,
                    success: function (data) {

                        // Return response
                        console.log(data);
                        def.resolve(data);
                    },
                    error: function (ts) {
                        //alert(ts.responseText);
                        //alert(ts.responseStatus);
                    }
                });
                return def.promise();
            }
        },

        fields: [
            {
                name: "designId", title: "ID", type: "text", validate: "required", width: 60,
                editTemplate: function (value, item) {
                    var $input = jsGrid.fields.text.prototype.editTemplate.apply(this, arguments);
                    return $input.attr('class', 'form-control').attr('value', value);
                },
                insertTemplate: function (value, item) {
                    var $input = jsGrid.fields.text.prototype.insertTemplate.apply(this, arguments);
                    return $input.attr('class', 'form-control').attr('value', value);
                }
            },

            {
                name: "technologyId", title: "Technology", type: "select", items: technologies, valueField: "technologyId", textField: "name",
                editTemplate: function (value, item) {
                    var $select = jsGrid.fields.select.prototype.editTemplate.apply(this, arguments);
                    return $select.attr('class', 'form-control');
                },
                insertTemplate: function (value, item) {
                    var $input = jsGrid.fields.select.prototype.insertTemplate.apply(this, arguments);
                    return $input.attr('class', 'form-control').attr('value', value);
                }
            },

            {
                name: "tap", title: "Tap [%]", type: "number",
                editTemplate: function (value, item) {
                    var $input = jsGrid.fields.text.prototype.editTemplate.apply(this, arguments);
                    return $input.attr('class', 'form-control').attr('value', value);
                },
                insertTemplate: function (value, item) {
                    var $input = jsGrid.fields.text.prototype.insertTemplate.apply(this, arguments);
                    return $input.attr('class', 'form-control').attr('value', value);
                }
            },

            {
                name: "circuitId", type: "number", visible: false
            },
            {
                name: "windingId", type: "number", visible: false
            },


            // Generic template
            {
                type: "control",
                itemTemplate: function (value, item) {
                    var $result = $([]);

                    var $deleteButton = $(this._createDeleteButton(item));

                    //$deleteButton.css("background-image", "none").val("&#xf1f8;");

                    // Just add edit icon, not delete
                    $result = $result.add($deleteButton);

                    return $result;
                }
            }
        ]
    });
}

function getWindingsData(circuitId) {
    // Get windings of selected circuit
    $.ajax({
        type: "GET",
        data: { id: circuitId },
        url: '/Circuits/Windings',
        success: function (data) {
            return data;
        },
        error: function (status) {
            //alert("Error");
            return null;
        }
    });
}

function getTechnologiesData() {
    // Get technologies
    $.ajax({
        type: "GET",
        dataType: "json",
        url: '/technologies',
        success: function (data) {
            technologies = data;
        },
        error: function (status) {
            //alert("Error");
            technologies = null;
        }
    });
}


function initializeMechGroups() {
    $(".mech-groups-grid").jsGrid({
        width: "100%",

        inserting: true,
        editing: true,
        sorting: true,
        //paging: true,

        invalidMessage: "Invalid data entered",

        editButton: false,

        data: mechGroups,

        fields: [
            {
                name: "ID", type: "number", validate: "required", width: 60,    
                editTemplate: function (value, item) {
                    var $input = jsGrid.fields.text.prototype.editTemplate.apply(this, arguments);
                    return $input.attr('class', 'form-control').attr('value', value);
                },
                insertTemplate: function (value, item) {
                    var $input = jsGrid.fields.text.prototype.insertTemplate.apply(this, arguments);
                    return $input.attr('class', 'form-control').attr('value', value);
                }
            },

            {
                name: "Top Insulation", type: "number", validate: "required",
                editTemplate: function (value, item) {
                    var $input = jsGrid.fields.text.prototype.editTemplate.apply(this, arguments);
                    return $input.attr('class', 'form-control').attr('value', value);
                },
                insertTemplate: function (value, item) {
                    var $input = jsGrid.fields.text.prototype.insertTemplate.apply(this, arguments);
                    return $input.attr('class', 'form-control').attr('value', value);
                }
            },

            {
                name: "Bottom Insulation", type: "number", validate: "required",
                editTemplate: function (value, item) {
                    var $input = jsGrid.fields.text.prototype.editTemplate.apply(this, arguments);
                    return $input.attr('class', 'form-control').attr('value', value);
                },
                insertTemplate: function (value, item) {
                    var $input = jsGrid.fields.text.prototype.insertTemplate.apply(this, arguments);
                    return $input.attr('class', 'form-control').attr('value', value);
                }
            },

            {
                name: "Inner Insulation", type: "number", validate: "required",
                editTemplate: function (value, item) {
                    var $input = jsGrid.fields.text.prototype.editTemplate.apply(this, arguments);
                    return $input.attr('class', 'form-control').attr('value', value);
                },
                insertTemplate: function (value, item) {
                    var $input = jsGrid.fields.text.prototype.insertTemplate.apply(this, arguments);
                    return $input.attr('class', 'form-control').attr('value', value);
                }
            },

            {
                name: "Inner Insulation", type: "number", validate: "required",
                editTemplate: function (value, item) {
                    var $input = jsGrid.fields.text.prototype.editTemplate.apply(this, arguments);
                    return $input.attr('class', 'form-control').attr('value', value);
                },
                insertTemplate: function (value, item) {
                    var $input = jsGrid.fields.text.prototype.insertTemplate.apply(this, arguments);
                    return $input.attr('class', 'form-control').attr('value', value);
                }
            },

            {
                name: "Material", type: "select", items: materials, valueField: "Id", textField: "Name",
                editTemplate: function (value, item) {
                    var $select = jsGrid.fields.select.prototype.editTemplate.apply(this, arguments);
                    return $select.attr('class', 'form-control');
                },
                insertTemplate: function (value, item) {
                    var $input = jsGrid.fields.select.prototype.insertTemplate.apply(this, arguments);
                    return $input.attr('class', 'form-control').attr('value', value);
                }
            },

            {
                name: "Windings Contained", type: "text", readOnly: true,
                editTemplate: function (value, item) {
                    var $input = jsGrid.fields.text.prototype.editTemplate.apply(this, arguments);
                    return $input.attr('class', 'form-control').attr('value', value);
                },
                insertTemplate: function (value, item) {
                    var $input = jsGrid.fields.text.prototype.insertTemplate.apply(this, arguments);
                    return $input.attr('class', 'form-control').attr('value', value);
                }
            },

            // Generic template
            {
                type: "control",
                itemTemplate: function (value, item) {
                    var $result = $([]);

                    var $deleteButton = $(this._createDeleteButton(item));

                    //$deleteButton.css("background-image", "none").val("&#xf1f8;");

                    // Just add edit icon, not delete
                    $result = $result.add($deleteButton);

                    return $result;
                }
            }
        ]
    });
}


function initializeBarriers() {
    $(".barriers-grid").jsGrid({
        width: "100%",

        inserting: true,
        editing: true,
        sorting: true,

        invalidMessage: "Invalid data entered",

        editButton: false,

        data: mechGroups,

        fields: [
            {
                name: "ID", type: "number", validate: "required", width: 60,
                editTemplate: function (value, item) {
                    var $input = jsGrid.fields.text.prototype.editTemplate.apply(this, arguments);
                    return $input.attr('class', 'form-control').attr('value', value);
                },
                insertTemplate: function (value, item) {
                    var $input = jsGrid.fields.text.prototype.insertTemplate.apply(this, arguments);
                    return $input.attr('class', 'form-control').attr('value', value);
                }
            },

            {
                name: "Top Insulation", type: "number", validate: "required",
                editTemplate: function (value, item) {
                    var $input = jsGrid.fields.text.prototype.editTemplate.apply(this, arguments);
                    return $input.attr('class', 'form-control').attr('value', value);
                },
                insertTemplate: function (value, item) {
                    var $input = jsGrid.fields.text.prototype.insertTemplate.apply(this, arguments);
                    return $input.attr('class', 'form-control').attr('value', value);
                }
            },

            {
                name: "Bottom Insulation", type: "number", validate: "required",
                editTemplate: function (value, item) {
                    var $input = jsGrid.fields.text.prototype.editTemplate.apply(this, arguments);
                    return $input.attr('class', 'form-control').attr('value', value);
                },
                insertTemplate: function (value, item) {
                    var $input = jsGrid.fields.text.prototype.insertTemplate.apply(this, arguments);
                    return $input.attr('class', 'form-control').attr('value', value);
                }
            },

            {
                name: "Inner Insulation", type: "number", validate: "required",
                editTemplate: function (value, item) {
                    var $input = jsGrid.fields.text.prototype.editTemplate.apply(this, arguments);
                    return $input.attr('class', 'form-control').attr('value', value);
                },
                insertTemplate: function (value, item) {
                    var $input = jsGrid.fields.text.prototype.insertTemplate.apply(this, arguments);
                    return $input.attr('class', 'form-control').attr('value', value);
                }
            },

            {
                name: "Inner Insulation", type: "number", validate: "required",
                editTemplate: function (value, item) {
                    var $input = jsGrid.fields.text.prototype.editTemplate.apply(this, arguments);
                    return $input.attr('class', 'form-control').attr('value', value);
                },
                insertTemplate: function (value, item) {
                    var $input = jsGrid.fields.text.prototype.insertTemplate.apply(this, arguments);
                    return $input.attr('class', 'form-control').attr('value', value);
                }
            },

            {
                name: "Material", type: "select", items: materials, valueField: "Id", textField: "Name",
                editTemplate: function (value, item) {
                    var $select = jsGrid.fields.select.prototype.editTemplate.apply(this, arguments);
                    return $select.attr('class', 'form-control');
                },
                insertTemplate: function (value, item) {
                    var $input = jsGrid.fields.select.prototype.insertTemplate.apply(this, arguments);
                    return $input.attr('class', 'form-control').attr('value', value);
                }
            },

            {
                name: "Windings Contained", type: "text", readOnly: true,
                editTemplate: function (value, item) {
                    var $input = jsGrid.fields.text.prototype.editTemplate.apply(this, arguments);
                    return $input.attr('class', 'form-control').attr('value', value);
                },
                insertTemplate: function (value, item) {
                    var $input = jsGrid.fields.text.prototype.insertTemplate.apply(this, arguments);
                    return $input.attr('class', 'form-control').attr('value', value);
                }
            },

            // Generic template
            {
                type: "control",
                itemTemplate: function (value, item) {
                    var $result = $([]);

                    var $deleteButton = $(this._createDeleteButton(item));

                    //$deleteButton.css("background-image", "none").val("&#xf1f8;");

                    // Just add edit icon, not delete
                    $result = $result.add($deleteButton);

                    return $result;
                }
            }
        ]
    });
}


$(function () {
    getTechnologiesData();
    //reloadWindings();
    initializeMechGroups();
});
