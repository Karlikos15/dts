﻿// Get technologies select 
var technologies;

var tabPosition;
var impedances = [
    { "ID": "1", "Feeders": "C1", "Shortcircuited": "C2", "Xcc Windings": 6.4, "Rcc Windings": 0.64, "Xcc Busbars": 0.39, "Rcc Busbars": 0.15, "Zcc": 6.8 },
    { "ID": "2", "Feeders": "C1", "Shortcircuited": "C2", "Xcc Windings": 6.4, "Rcc Windings": 0.64, "Xcc Busbars": 0.39, "Rcc Busbars": 0.15, "Zcc": 6.8 },
    { "ID": "3", "Feeders": "C1", "Shortcircuited": "C2", "Xcc Windings": 6.4, "Rcc Windings": 0.64, "Xcc Busbars": 0.39, "Rcc Busbars": 0.15, "Zcc": 6.8 },
    { "ID": "4", "Feeders": "C1", "Shortcircuited": "C2", "Xcc Windings": 6.4, "Rcc Windings": 0.64, "Xcc Busbars": 0.39, "Rcc Busbars": 0.15, "Zcc": 6.8 },
    { "ID": "5", "Feeders": "C1", "Shortcircuited": "C2", "Xcc Windings": 6.4, "Rcc Windings": 0.64, "Xcc Busbars": 0.39, "Rcc Busbars": 0.15, "Zcc": 6.8 },
    { "ID": "6", "Feeders": "C1", "Shortcircuited": "C2", "Xcc Windings": 6.4, "Rcc Windings": 0.64, "Xcc Busbars": 0.39, "Rcc Busbars": 0.15, "Zcc": 6.8 },
    { "ID": "7", "Feeders": "C1", "Shortcircuited": "C2", "Xcc Windings": 6.4, "Rcc Windings": 0.64, "Xcc Busbars": 0.39, "Rcc Busbars": 0.15, "Zcc": 6.8 },
    { "ID": "8", "Feeders": "C1", "Shortcircuited": "C2", "Xcc Windings": 6.4, "Rcc Windings": 0.64, "Xcc Busbars": 0.39, "Rcc Busbars": 0.15, "Zcc": 6.8 },
    { "ID": "9", "Feeders": "C1", "Shortcircuited": "C2", "Xcc Windings": 6.4, "Rcc Windings": 0.64, "Xcc Busbars": 0.39, "Rcc Busbars": 0.15, "Zcc": 6.8 },
    { "ID": "10", "Feeders": "C1", "Shortcircuited": "C2", "Xcc Windings": 6.4, "Rcc Windings": 0.64, "Xcc Busbars": 0.39, "Rcc Busbars": 0.15, "Zcc": 6.8 },
    { "ID": "11", "Feeders": "C1", "Shortcircuited": "C2", "Xcc Windings": 6.4, "Rcc Windings": 0.64, "Xcc Busbars": 0.39, "Rcc Busbars": 0.15, "Zcc": 6.8 },
    { "ID": "12", "Feeders": "C1", "Shortcircuited": "C2", "Xcc Windings": 6.4, "Rcc Windings": 0.64, "Xcc Busbars": 0.39, "Rcc Busbars": 0.15, "Zcc": 6.8 },
    { "ID": "13", "Feeders": "C1", "Shortcircuited": "C2", "Xcc Windings": 6.4, "Rcc Windings": 0.64, "Xcc Busbars": 0.39, "Rcc Busbars": 0.15, "Zcc": 6.8 },
    { "ID": "14", "Feeders": "C1", "Shortcircuited": "C2", "Xcc Windings": 6.4, "Rcc Windings": 0.64, "Xcc Busbars": 0.39, "Rcc Busbars": 0.15, "Zcc": 6.8 },
    { "ID": "15", "Feeders": "C1", "Shortcircuited": "C2", "Xcc Windings": 6.4, "Rcc Windings": 0.64, "Xcc Busbars": 0.39, "Rcc Busbars": 0.15, "Zcc": 6.8 },
    { "ID": "16", "Feeders": "C1", "Shortcircuited": "C2", "Xcc Windings": 6.4, "Rcc Windings": 0.64, "Xcc Busbars": 0.39, "Rcc Busbars": 0.15, "Zcc": 6.8 },
    { "ID": "17", "Feeders": "C1", "Shortcircuited": "C2", "Xcc Windings": 6.4, "Rcc Windings": 0.64, "Xcc Busbars": 0.39, "Rcc Busbars": 0.15, "Zcc": 6.8 },
    { "ID": "18", "Feeders": "C1", "Shortcircuited": "C2", "Xcc Windings": 6.4, "Rcc Windings": 0.64, "Xcc Busbars": 0.39, "Rcc Busbars": 0.15, "Zcc": 6.8 },
    { "ID": "19", "Feeders": "C1", "Shortcircuited": "C2", "Xcc Windings": 6.4, "Rcc Windings": 0.64, "Xcc Busbars": 0.39, "Rcc Busbars": 0.15, "Zcc": 6.8 },
    { "ID": "20", "Feeders": "C1", "Shortcircuited": "C2", "Xcc Windings": 6.4, "Rcc Windings": 0.64, "Xcc Busbars": 0.39, "Rcc Busbars": 0.15, "Zcc": 6.8 },
    
];

var materials = [
    { Name: "Epoxy", Id: 0 },
    { Name: "Impregnated", Id: 1 },
    { Name: "OtherMechGroupFiller", Id: 2 }
];


function reloadWindings(tabId) {
    tabPosition = tabId;
    $(".windings-circuit-grid").jsGrid({
        width: "100%",

        inserting: true,
        editing: true,
        sorting: true,
        autoload: true,
        //paging: true,

        invalidMessage: "Invalid data entered",

        editButton: false,
        controller: {
            loadData: function (filter) {
                var def = $.Deferred();
                // Get windings of selected circuit
                $.ajax({
                    type: "GET",
                    dataType: "json",
                    url: '/windings/' + tabPosition.toString(),
                    success: function (data) {

                        // Return response
                        console.log(data);
                        def.resolve(data);
                    },
                    error: function (ts) {
                        //alert("Error");
                    }
                });
                return def.promise();
            },
            insertItem: function (item) {
                var def = $.Deferred();

                // Get windings of selected circuit
                $.ajax({
                    type: "POST",
                    dataType: "json",
                    data: { winding: item, tabPosition: tabPosition },
                    url: '/windings',
                    success: function (data) {

                        // Return response
                        console.log(data);
                        def.resolve(data);
                    },
                    error: function (ts) {
                        //alert(ts.responseText);
                        //alert(ts.responseStatus);
                    }
                });
                return def.promise();
            },
            updateItem: function (item) {
                var def = $.Deferred();
                // Get windings of selected circuit
                $.ajax({
                    type: "PUT",
                    dataType: "json",
                    data: { winding: item, tabPosition: tabPosition },
                    url: '/windings/' + item.windingId,
                    success: function (data) {

                        // Return response
                        console.log(data);
                        def.resolve(data);
                    },
                    error: function (ts) {
                        //alert(ts.responseText);
                        //alert(ts.responseStatus);
                    }
                });
                return def.promise();
            },
            deleteItem: function (item) {
                var def = $.Deferred();
                // Get windings of selected circuit
                $.ajax({
                    type: "DELETE",
                    url: '/windings/' + item.windingId,
                    success: function (data) {

                        // Return response
                        console.log(data);
                        def.resolve(data);
                    },
                    error: function (ts) {
                        //alert(ts.responseText);
                        //alert(ts.responseStatus);
                    }
                });
                return def.promise();
            }
        },

        fields: [
            {
                name: "designId", title: "ID", type: "text", validate: "required", width: 60,
                editTemplate: function (value, item) {
                    var $input = jsGrid.fields.text.prototype.editTemplate.apply(this, arguments);
                    return $input.attr('class', 'form-control').attr('value', value);
                },
                insertTemplate: function (value, item) {
                    var $input = jsGrid.fields.text.prototype.insertTemplate.apply(this, arguments);
                    return $input.attr('class', 'form-control').attr('value', value);
                }
            },

            {
                name: "technologyId", title: "Technology", type: "select", items: technologies, valueField: "technologyId", textField: "name",
                editTemplate: function (value, item) {
                    var $select = jsGrid.fields.select.prototype.editTemplate.apply(this, arguments);
                    return $select.attr('class', 'form-control');
                },
                insertTemplate: function (value, item) {
                    var $input = jsGrid.fields.select.prototype.insertTemplate.apply(this, arguments);
                    return $input.attr('class', 'form-control').attr('value', value);
                }
            },

            {
                name: "tap", title: "Tap [%]", type: "number",
                editTemplate: function (value, item) {
                    var $input = jsGrid.fields.text.prototype.editTemplate.apply(this, arguments);
                    return $input.attr('class', 'form-control').attr('value', value);
                },
                insertTemplate: function (value, item) {
                    var $input = jsGrid.fields.text.prototype.insertTemplate.apply(this, arguments);
                    return $input.attr('class', 'form-control').attr('value', value);
                }
            },

            {
                name: "circuitId", type: "number", visible: false
            },
            {
                name: "windingId", type: "number", visible: false
            },


            // Generic template
            {
                type: "control",
                itemTemplate: function (value, item) {
                    var $result = $([]);

                    var $deleteButton = $(this._createDeleteButton(item));

                    //$deleteButton.css("background-image", "none").val("&#xf1f8;");

                    // Just add edit icon, not delete
                    $result = $result.add($deleteButton);

                    return $result;
                }
            }
        ]
    });
}

function getWindingsData(circuitId) {
    // Get windings of selected circuit
    $.ajax({
        type: "GET",
        data: { id: circuitId },
        url: '/Circuits/Windings',
        success: function (data) {
            return data;
        },
        error: function (status) {
            //alert("Error");
            return null;
        }
    });
}

function getTechnologiesData() {
    // Get technologies
    $.ajax({
        type: "GET",
        dataType: "json",
        url: '/technologies',
        success: function (data) {
            technologies = data;
        },
        error: function (status) {
            //alert("Error");
            technologies = null;
        }
    });
}


function initializeImpedances() {
    $(".impedances-grid").jsGrid({
        width: "100%",

        inserting: true,
        editing: true,
        sorting: true,
        //paging: true,

        invalidMessage: "Invalid data entered",

        editButton: false,

        data: impedances,

        fields: [
            {
                name: "Feeders", type: "text", validate: "required", width: 60,
                editTemplate: function (value, item) {
                    var $input = jsGrid.fields.text.prototype.editTemplate.apply(this, arguments);
                    return $input.attr('class', 'form-control').attr('value', value);
                },
                insertTemplate: function (value, item) {
                    var $input = jsGrid.fields.text.prototype.insertTemplate.apply(this, arguments);
                    return $input.attr('class', 'form-control').attr('value', value);
                }
            },

            {
                name: "Shortcircuited", type: "text", validate: "required",
                editTemplate: function (value, item) {
                    var $input = jsGrid.fields.text.prototype.editTemplate.apply(this, arguments);
                    return $input.attr('class', 'form-control').attr('value', value);
                },
                insertTemplate: function (value, item) {
                    var $input = jsGrid.fields.text.prototype.insertTemplate.apply(this, arguments);
                    return $input.attr('class', 'form-control').attr('value', value);
                }
            },

            {
                name: "Xcc Windings[%]", type: "number", validate: "required",
                editTemplate: function (value, item) {
                    var $input = jsGrid.fields.text.prototype.editTemplate.apply(this, arguments);
                    return $input.attr('class', 'form-control').attr('value', value);
                },
                insertTemplate: function (value, item) {
                    var $input = jsGrid.fields.text.prototype.insertTemplate.apply(this, arguments);
                    return $input.attr('class', 'form-control').attr('value', value);
                }
            },

            {
                name: "Rcc Windings[%]", type: "number", validate: "required",
                editTemplate: function (value, item) {
                    var $input = jsGrid.fields.text.prototype.editTemplate.apply(this, arguments);
                    return $input.attr('class', 'form-control').attr('value', value);
                },
                insertTemplate: function (value, item) {
                    var $input = jsGrid.fields.text.prototype.insertTemplate.apply(this, arguments);
                    return $input.attr('class', 'form-control').attr('value', value);
                }
            },
            {
                name: "Xcc Busbars[%]", type: "number", validate: "required",
                editTemplate: function (value, item) {
                    var $input = jsGrid.fields.text.prototype.editTemplate.apply(this, arguments);
                    return $input.attr('class', 'form-control').attr('value', value);
                },
                insertTemplate: function (value, item) {
                    var $input = jsGrid.fields.text.prototype.insertTemplate.apply(this, arguments);
                    return $input.attr('class', 'form-control').attr('value', value);
                }
            },

            {
                name: "Rcc Busbars[%]", type: "number", validate: "required",
                editTemplate: function (value, item) {
                    var $input = jsGrid.fields.text.prototype.editTemplate.apply(this, arguments);
                    return $input.attr('class', 'form-control').attr('value', value);
                },
                insertTemplate: function (value, item) {
                    var $input = jsGrid.fields.text.prototype.insertTemplate.apply(this, arguments);
                    return $input.attr('class', 'form-control').attr('value', value);
                }
            },
            
            {
                name: "Zcc [%]", type: "number", items: materials, valueField: "Id", textField: "Name",
                editTemplate: function (value, item) {
                    var $select = jsGrid.fields.select.prototype.editTemplate.apply(this, arguments);
                    return $select.attr('class', 'form-control');
                },
                insertTemplate: function (value, item) {
                    var $input = jsGrid.fields.select.prototype.insertTemplate.apply(this, arguments);
                    return $input.attr('class', 'form-control').attr('value', value);
                }
            },

            {
                name: "Flux Paths", type: "text", readOnly: true,
                itemTemplate: function (value, item) {
                    return $("<button>").text("Click to see")
                        .on("click", function () {
                            show(item.watch());
                        });
                }
            },

            // Generic template
            {
                type: "control",
                itemTemplate: function (value, item) {
                    var $result = $([]);

                    var $deleteButton = $(this._createDeleteButton(item));
                    var $editButton = $(this._createEditButton(item));

                    //$deleteButton.css("background-image", "image('')").val("&#xf1f8;");
                    $editButton.css("class", "glyphicon glyphicon-pencil");

                    // Just add edit icon, not delete

                    //$result = $result.add($deleteButton);
                    $result = $result.add($editButton);

                    return $result;
                }
            }
        ]
    });
}


function initializeBarriers() {
    $(".barriers-grid").jsGrid({
        width: "100%",

        inserting: true,
        editing: true,
        sorting: true,

        invalidMessage: "Invalid data entered",

        editButton: false,

        data: mechGroups,

        fields: [
            {
                name: "ID", type: "number", validate: "required", width: 60,
                editTemplate: function (value, item) {
                    var $input = jsGrid.fields.text.prototype.editTemplate.apply(this, arguments);
                    return $input.attr('class', 'form-control').attr('value', value);
                },
                insertTemplate: function (value, item) {
                    var $input = jsGrid.fields.text.prototype.insertTemplate.apply(this, arguments);
                    return $input.attr('class', 'form-control').attr('value', value);
                }
            },

            {
                name: "Top Insulation", type: "number", validate: "required",
                editTemplate: function (value, item) {
                    var $input = jsGrid.fields.text.prototype.editTemplate.apply(this, arguments);
                    return $input.attr('class', 'form-control').attr('value', value);
                },
                insertTemplate: function (value, item) {
                    var $input = jsGrid.fields.text.prototype.insertTemplate.apply(this, arguments);
                    return $input.attr('class', 'form-control').attr('value', value);
                }
            },

            {
                name: "Bottom Insulation", type: "number", validate: "required",
                editTemplate: function (value, item) {
                    var $input = jsGrid.fields.text.prototype.editTemplate.apply(this, arguments);
                    return $input.attr('class', 'form-control').attr('value', value);
                },
                insertTemplate: function (value, item) {
                    var $input = jsGrid.fields.text.prototype.insertTemplate.apply(this, arguments);
                    return $input.attr('class', 'form-control').attr('value', value);
                }
            },

            {
                name: "Inner Insulation", type: "number", validate: "required",
                editTemplate: function (value, item) {
                    var $input = jsGrid.fields.text.prototype.editTemplate.apply(this, arguments);
                    return $input.attr('class', 'form-control').attr('value', value);
                },
                insertTemplate: function (value, item) {
                    var $input = jsGrid.fields.text.prototype.insertTemplate.apply(this, arguments);
                    return $input.attr('class', 'form-control').attr('value', value);
                }
            },

            {
                name: "Inner Insulation", type: "number", validate: "required",
                editTemplate: function (value, item) {
                    var $input = jsGrid.fields.text.prototype.editTemplate.apply(this, arguments);
                    return $input.attr('class', 'form-control').attr('value', value);
                },
                insertTemplate: function (value, item) {
                    var $input = jsGrid.fields.text.prototype.insertTemplate.apply(this, arguments);
                    return $input.attr('class', 'form-control').attr('value', value);
                }
            },

            {
                name: "Material", type: "select", items: materials, valueField: "Id", textField: "Name",
                editTemplate: function (value, item) {
                    var $select = jsGrid.fields.select.prototype.editTemplate.apply(this, arguments);
                    return $select.attr('class', 'form-control');
                },
                insertTemplate: function (value, item) {
                    var $input = jsGrid.fields.select.prototype.insertTemplate.apply(this, arguments);
                    return $input.attr('class', 'form-control').attr('value', value);
                }
            },

            {
                name: "Windings Contained", type: "text", readOnly: true,
                editTemplate: function (value, item) {
                    var $input = jsGrid.fields.text.prototype.editTemplate.apply(this, arguments);
                    return $input.attr('class', 'form-control').attr('value', value);
                },
                insertTemplate: function (value, item) {
                    var $input = jsGrid.fields.text.prototype.insertTemplate.apply(this, arguments);
                    return $input.attr('class', 'form-control').attr('value', value);
                }
            },

            // Generic template
            {
                type: "control",
                itemTemplate: function (value, item) {
                    var $result = $([]);

                    var $deleteButton = $(this._createDeleteButton(item));

                    //$deleteButton.css("background-image", "none").val("&#xf1f8;");

                    // Just add edit icon, not delete
                    $result = $result.add($deleteButton);

                    return $result;
                }
            }
        ]
    });
}


$(function () {
    getTechnologiesData();
    //reloadWindings();
    initializeImpedances();
});
