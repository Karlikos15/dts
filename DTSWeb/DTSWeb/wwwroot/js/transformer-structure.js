﻿/******************  P5.JS DRAWING  ******************/
var transformerSketch = function(p) {
    p.setup = function () {
        // Increase resolution
        var increment = 15;

        /* Canvas specs */

        // Size
        var canvasWidth = 360;

        var canvasHeight = canvasWidth + 26;

        /************************/

        /* Main container specs */

        // Origin
        var mainContainerX = 20;
        var mainContainerY = 35;

        // Size
        var mainContainerWidth = 320;
        var mainContainerHeight = 320;

        // Round corners
        var mainContainerBorderRadius = 5;

        /************************/

        /* Core specs */

        // Origin
        var coreX = 10 + mainContainerX;
        var coreY = 10 + mainContainerY;

        // Size
        var coreWidth = 50;
        var coreHeight = mainContainerHeight - 20;

        // Round corners
        var coreBorderRadius = 5;

        /**************/

        /* Mech group 1 specs */

        // Origin
        var mechGroup1X = 10 + coreX + coreWidth;
        var mechGroup1Y = coreY;

        // Size
        var mechGroup1Width = 240;
        var mechGroup1Height = 140;

        // Round corners
        var mechGroup1BorderRadius = 5;

        /**************/

        /* Mech group 2 specs */

        // Origin
        var mechGroup2X = 10 + coreX + coreWidth;
        var mechGroup2Y = mechGroup1Y + mechGroup1Height + 20;

        // Size
        var mechGroup2Width = 240;
        var mechGroup2Height = 140;

        // Round corners
        var mechGroup2BorderRadius = 5;

        /**************/

        /**************/

        /* Winding1 specs */

        // Origin
        var winding1X = 10 + mechGroup1X;
        var winding1Y = 10 + mechGroup1Y;

        // Size
        var winding1Width = mechGroup1Width / 2 - 10;
        var winding1Height = mechGroup1Height / 2;

        // Round corners
        var winding1BorderRadius = 5;

        /**************/

        /* Winding2 specs */

        // Origin
        var winding2X = 10 + winding1X + winding1Width;
        var winding2Y = winding1Y + winding1Height / 2;

        // Size
        var winding2Width = mechGroup1Width / 2 - 20;
        var winding2Height = mechGroup1Height / 2;

        // Round corners
        var winding2BorderRadius = 5;

        /**************/

        /* Winding3 specs */

        // Origin
        var winding3X = 10 + mechGroup2X;
        var winding3Y = 10 + mechGroup2Y;

        // Size
        var winding3Width = mechGroup2Width - 20;
        var winding3Height = mechGroup2Height - 20;

        // Round corners
        var winding3BorderRadius = 5;

        /**************/

        // Create the canvas
        var coreCanvas = p.createCanvas(canvasWidth * increment, canvasHeight * increment);

        // No stroke
        p.noStroke();

        // Position the canvas into <div id="transformer-structure">
        coreCanvas.parent('transformer-structure-container');

        // Set background color
        p.background('#C6C5C5');

        // Set colors (red)
        p.fill(255, 0, 0, 100);
        //p.stroke(150, 0, 0);

        // Draw main container
        p.rect(mainContainerX * increment,
            mainContainerY * increment,
            mainContainerWidth * increment,
            mainContainerHeight * increment,
            mainContainerBorderRadius * increment);

        // Set colors (red)
        p.fill(255, 0, 0, 140);
        //p.stroke(150, 0, 0);

        // Draw core
        p.rect(coreX * increment,
            coreY * increment,
            coreWidth * increment,
            coreHeight * increment,
            coreBorderRadius * increment);

        /**
         * TOP MECH GROUP
         */

        // Set colors (orange)
        p.fill(255, 50, 0, 120);
        //p.stroke(245, 40, 0);

        // Draw mechanical group
        p.rect(mechGroup1X * increment,
            mechGroup1Y * increment,
            mechGroup1Width * increment,
            mechGroup1Height * increment,
            mechGroup1BorderRadius * increment);

        // Set colors (green)
        p.fill(0, 200, 0, 140);
        //p.stroke(0, 150, 0);

        // Draw winding1
        p.rect(winding1X * increment,
            winding1Y * increment,
            winding1Width * increment,
            winding1Height * increment,
            winding1BorderRadius * increment);

        // Draw winding2
        p.rect(winding2X * increment,
            winding2Y * increment,
            winding2Width * increment,
            winding2Height * increment,
            winding2BorderRadius * increment);

        /**
         * BOTTOM MECH GROUP
         */

        // Set colors (orange)
        p.fill(255, 50, 0, 120);
        //p.stroke(245, 40, 0);

        // Draw mechanical group
        p.rect(mechGroup2X * increment,
            mechGroup2Y * increment,
            mechGroup2Width * increment,
            mechGroup2Height * increment,
            mechGroup2BorderRadius * increment);

        // Set colors (green)
        p.fill(0, 200, 0, 140);
        //p.stroke(0, 150, 0);

        // Draw winding3
        p.rect(winding3X * increment,
            winding3Y * increment,
            winding3Width * increment,
            winding3Height * increment,
            winding3BorderRadius * increment);
    };

};

var transformerInstance = new p5(transformerSketch);