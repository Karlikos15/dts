﻿var coreInstance;

// Increase resolution
var increment = 2;

/******************  P5.JS DRAWING  ******************/
function createCoreStructure(shape) {
    // Remove previous core
    if (coreInstance != null) {
        coreInstance.remove();
    }


    var coreSketch = function(p) {
        p.setup = function() {


            /* Canvas specs */

            // Size
            var canvasWidth = 410;

            var canvasHeight = 380;


            // Create the canvas
            var coreCanvas = p.createCanvas(canvasWidth * increment, canvasHeight * increment);

            // Position the canvas into <div id="core-structure-container">
            coreCanvas.parent('core-structure-container');


            // Set colors (red)
            //p.fill(255, 0, 0, 100);
            //p.stroke(150, 0, 0);

            p.noFill();
            p.stroke(100, 100, 100);
            p.strokeWeight(increment);

            // Switch core shape
            switch (shape) {
            case 1:
                // Draw round core
                drawRoundCore(p);
                break;
            case 2:
                // Draw two step core
                drawTwoStepCore(p);
                break;
            case 3:
                // Draw two step core
                drawOneStepCore(p);
                break;
            case 4:
                // Draw two step core
                drawOvalCore(p);
                break;
            }

        };

    };

    // Initialize
    coreInstance = new p5(coreSketch);
}


function drawRoundCore(p) {
    /************************/

    /* Main container specs */

    // Origin
    var mainContainerX = 200;
    var mainContainerY = 200;

    // Size
    var mainContainerWidth = 320;
    var mainContainerHeight = 320;

    /************************/

    /* Rectangle0 specs */

    // Origin
    var rect0X = 107;
    var rect0Y = 70;

    // Size
    var rect0Width = 185;
    var rect0Height = 260;

    /**************/

    /************************/

    /* Rectangle1 specs */

    // Origin
    var rect1X = 87;
    var rect1Y = 87;

    // Size
    var rect1Width = 226;
    var rect1Height = 226;

    /**************/


    /************************/

    /* Rectangle2 specs */

    // Origin
    var rect2X = 67;
    var rect2Y = 110;

    // Size
    var rect2Width = 265;
    var rect2Height = 180;


    /**************/

    /* Rectangle3 specs */

    // Origin
    var rect3X = 57;
    var rect3Y = 130;

    // Size
    var rect3Width = 287;
    var rect3Height = 137;


    /**************/

    /**************/

    /* Line specs */

    // Origin
    var lineX1 = 10;
    var lineY1 = 200;

    // Size
    var lineX2 = 390;
    var lineY2 = 200;


    /**************/

    // Draw ellipse
    p.ellipse(mainContainerX * increment,
        mainContainerY * increment,
        mainContainerWidth * increment,
        mainContainerHeight * increment);

    //p.fill('#FF8686');
    //p.noStroke();
    ////p.stroke(0, 0, 0);
    ////p.strokeWeight(3);

    //// Draw rectangle0
    //p.rect(rect0X * increment, rect0Y * increment, rect0Width * increment, rect0Height * increment);

    //// Set color red 1
    //p.fill('#FF5D5D');

    //// Draw rectangle1
    //p.rect(rect1X * increment, rect1Y * increment, rect1Width * increment, rect1Height * increment);

    //// Set color red 2
    //p.fill('#EF3636'); 

    // No stroke
    p.noStroke();

    // Set color red 0
    p.fill('#FF8686'); 

    // Draw rectangle0
    p.rect(rect0X * increment, rect0Y * increment, rect0Width * increment, rect0Height * increment);

    // Set color red 1
    p.fill('#F86666');

    // Draw rectangle1
    p.rect(rect1X * increment, rect1Y * increment, rect1Width * increment, rect1Height * increment);

    // Set color red 2
    p.fill('#F24646'); 

    // Draw rectangle2
    p.rect(rect2X * increment, rect2Y * increment, rect2Width * increment, rect2Height * increment);

    // Set color red 3
    p.fill('#EF3636'); 


    // Draw rectangle3
    p.rect(rect3X * increment, rect3Y * increment, rect3Width * increment, rect3Height * increment);

    // Set stroke dark grey
    p.stroke(50, 50, 50);
    p.strokeWeight(increment);

    // Draw dashed line
    p.line(lineX1 * increment, lineY1 * increment, (lineX2 - 300) * increment, lineY2 * increment);
    p.line((lineX2 - 280) * increment, lineY1 * increment, (lineX2 - 250) * increment, lineY2 * increment);
    p.line((lineX2 - 230) * increment, lineY1 * increment, (lineX2 - 170) * increment, lineY2 * increment);
    p.line((lineX2 - 150) * increment, lineY1 * increment, (lineX2 - 110) * increment, lineY2 * increment);
    p.line((lineX2 - 90) * increment, lineY1 * increment, lineX2 * increment, lineY2 * increment);
}


function drawTwoStepCore(p) {


    /* Rectangle2 specs */

    // Origin
    var rect2X = 67;
    var rect2Y = 110;

    // Size
    var rect2Width = 265;
    var rect2Height = 172;


    /**************/

    /* Rectangle3 specs */

    // Origin
    var rect3X = 54;
    var rect3Y = 130;

    // Size
    var rect3Width = 290;
    var rect3Height = 135;


    /**************/

    /**************/

    /* Line specs */

    // Origin
    var lineX1 = 10;
    var lineY1 = 200;

    // Size
    var lineX2 = 390;
    var lineY2 = 200;

    // Round corners
    //var centerRectRadius = 5;


    /**************/

    // No stroke
    p.noStroke();

    // Set color red 0
    p.fill('#FF5D5D');

    // Draw rectangle2
    p.rect(rect2X * increment, rect2Y * increment, rect2Width * increment, rect2Height * increment);

    // Set color red 1
    p.fill('#EF3636'); 

    // Draw rectangle3
    p.rect(rect3X * increment, rect3Y * increment, rect3Width * increment, rect3Height * increment);


    // Set stroke dark grey
    p.stroke(50, 50, 50);
    p.strokeWeight(increment);

    // Draw dashed line
    p.line(lineX1 * increment, lineY1 * increment, (lineX2 - 300) * increment, lineY2 * increment);
    p.line((lineX2 - 280) * increment, lineY1 * increment, (lineX2 - 250) * increment, lineY2 * increment);
    p.line((lineX2 - 230) * increment, lineY1 * increment, (lineX2 - 170) * increment, lineY2 * increment);
    p.line((lineX2 - 150) * increment, lineY1 * increment, (lineX2 - 110) * increment, lineY2 * increment);
    p.line((lineX2 - 90) * increment, lineY1 * increment, lineX2 * increment, lineY2 * increment);
}


function drawOneStepCore(p) {

    /* Rectangle3 specs */

    // Origin
    var rect3X = 54;
    var rect3Y = 130;

    // Size
    var rect3Width = 290;
    var rect3Height = 135;


    /**************/

    /**************/

    /* Line specs */

    // Origin
    var lineX1 = 10;
    var lineY1 = 200;

    // Size
    var lineX2 = 390;
    var lineY2 = 200;

    // Round corners
    //var centerRectRadius = 5;


    /**************/

    // Set color red
    p.fill('#EF3636');

    // No stroke
    p.noStroke();

    // Draw rectangle3
    p.rect(rect3X * increment, rect3Y * increment, rect3Width * increment, rect3Height * increment);

    // Set stroke dark grey
    p.stroke(50, 50, 50);
    p.strokeWeight(increment);

    // Draw dashed line
    p.line(lineX1 * increment, lineY1 * increment, (lineX2 - 300) * increment, lineY2 * increment);
    p.line((lineX2 - 280) * increment, lineY1 * increment, (lineX2 - 250) * increment, lineY2 * increment);
    p.line((lineX2 - 230) * increment, lineY1 * increment, (lineX2 - 170) * increment, lineY2 * increment);
    p.line((lineX2 - 150) * increment, lineY1 * increment, (lineX2 - 110) * increment, lineY2 * increment);
    p.line((lineX2 - 90) * increment, lineY1 * increment, lineX2 * increment, lineY2 * increment);
}


function drawOvalCore(p) {
    /************************/

    /* Main container specs */

    // Origin
    var mainContainerX = 200;
    var mainContainerY = 200;

    // Size
    var mainContainerWidth = 300;
    var mainContainerHeight = 340;

    /************************/

    /* Rectangle0 specs */

    // Origin
    var rect0X = 120;
    var rect0Y = 57;

    // Size
    var rect0Width = 160;
    var rect0Height = 286;

    /**************/

    /************************/

    /* Rectangle1 specs */

    // Origin
    var rect1X = 90;
    var rect1Y = 85;

    // Size
    var rect1Width = 220;
    var rect1Height = 230;

    /**************/


    /************************/

    /* Rectangle2 specs */

    // Origin
    //var rect2X = 67;
    //var rect2Y = 110;

    //// Size
    //var rect2Width = 265;
    //var rect2Height = 180;


    /**************/

    /* Rectangle3 specs */

    // Origin
    var rect3X = 63;
    var rect3Y = 130;

    // Size
    var rect3Width = 274;
    var rect3Height = 137;


    /**************/

    /**************/

    /* Line specs */

    // Origin
    var lineX1 = 10;
    var lineY1 = 200;

    // Size
    var lineX2 = 390;
    var lineY2 = 200;


    /**************/

    // Draw ellipse
    p.ellipse(mainContainerX * increment, mainContainerY * increment, mainContainerWidth * increment, mainContainerHeight * increment);

    // Set color red 0
    p.fill('#FF8686');
    p.noStroke();
    //p.stroke(0, 0, 0);
    //p.strokeWeight(3);

    // Draw rectangle0
    p.rect(rect0X * increment, rect0Y * increment, rect0Width * increment, rect0Height * increment);

    // Set color red 1
    p.fill('#FF5D5D');

    // Draw rectangle1
    p.rect(rect1X * increment, rect1Y * increment, rect1Width * increment, rect1Height * increment);

    // Set color red 2
    p.fill('#EF3636'); 

    // Draw rectangle3
    p.rect(rect3X * increment, rect3Y * increment, rect3Width * increment, rect3Height * increment);

    // Set stroke dark grey
    p.stroke(50, 50, 50);
    p.strokeWeight(increment);

    // Draw dashed line
    p.line(lineX1 * increment, lineY1 * increment, (lineX2 - 300) * increment, lineY2 * increment);
    p.line((lineX2 - 280) * increment, lineY1 * increment, (lineX2 - 250) * increment, lineY2 * increment);
    p.line((lineX2 - 230) * increment, lineY1 * increment, (lineX2 - 170) * increment, lineY2 * increment);
    p.line((lineX2 - 150) * increment, lineY1 * increment, (lineX2 - 110) * increment, lineY2 * increment);
    p.line((lineX2 - 90) * increment, lineY1 * increment, lineX2 * increment, lineY2 * increment);
}

/********************    INITIALIZATION    ********************/
$(function() {
    createCoreStructure(1);
});