﻿/********************    INITIALIZATION    ********************/    
$(function () {

    /******** CUSTOM TOGGLES *********/
    $('#tapping-toggle').on('click',
        function () {
            if ($('#tapping-toggle').hasClass('fa-times')) {
                $('#tapping-toggle').removeClass('fa-times').addClass('fa-check').css('color', '#6A951D');
                $('#tapping-toggle').parent().parent().find('input,select').attr('disabled', false);
            } else {
                $('#tapping-toggle').removeClass('fa-check').addClass('fa-times').css('color', '#D9534F');
                $('#tapping-toggle').parent().parent().find('input,select').attr('disabled', true);
            }
        });

    $('#shielding-screen-toggle').on('click',
        function () {
            if ($('#shielding-screen-toggle').hasClass('fa-times')) {
                $('#shielding-screen-toggle').removeClass('fa-times').addClass('fa-check').css('color', '#6A951D');
                $('#shielding-screen-toggle').parent().parent().find('input,select').attr('disabled', false);
            } else {
                $('#shielding-screen-toggle').removeClass('fa-check').addClass('fa-times').css('color', '#D9534F');
                $('#shielding-screen-toggle').parent().parent().find('input,select').attr('disabled', true);
            }
        });

   /******** EVENTS *********/

   /* Draw core */
    $('#LegShapeId').change(function() {
        createCoreStructure(parseInt($(this).val()));
    });

    reloadFormsEvent();

    /* Generate circuit tabs on change */
    $('#circuits-main').change(function () {
        generateCircuits($(this).val());
    });

    /* Generate first circuit */
    generateCircuits(1);

    /* Create/Remove Core depending on ACR checkbox */
    $('#Acr').change(function () {
        if (this.checked) {
            hideCoreTab();
        } else {
            showCoreTab();
        }
        
    });
});

function generateCircuits(circuitsCount) {
    var circuits = circuitsCount;

    var pendingTabs = circuits - $('#circuits-container .tab-pane').length;

    // Avoid zero or negative indexes
    if (pendingTabs <= 0) {
        return;
    }

    // Generate tabs
    var tabId = parseInt(circuits) - pendingTabs + 1;

    for (var i = 0; i < pendingTabs; i++) {
        generateTab(tabId);
        tabId++;
    }

    // Show first tab
    getTab(1);
}

function showCoreTab() {
    // Show header tab 
    $('#core-middle-tab-header').show();

    // Remove active from selected tab
    $('#middle-tab-content').children().eq(1).removeClass('active');
    $('#middle-tab-headers').children().eq(1).removeClass('active');

    // Add active to core tab
    $('#core-middle-tab').addClass('active');
}

function hideCoreTab() {
    // Hide header tab 
    $('#core-middle-tab-header').hide();

    // Add active to first tab
    $('#middle-tab-content').children().eq(1).addClass('active');
    $('#middle-tab-headers').children().eq(1).addClass('active');

    // Remove active to core tab
    $('#core-middle-tab').removeClass('active');
}

function reloadFormsEvent() {
    /* Submit forms on change */
    $('input,select').change(function () {
        var form = this.closest("form");

        sendFormAjax(form, $('#' + $(form).data('response')));
    });
}

function sendFormAjax(form, responseInput) {
    $.ajax({
        type: "POST",
        data: $(form).serialize(),
        url: $(form).attr('action'),
        success: function (data) {
            $(responseInput).html(data);
        },
        error: function (status) {
            alert("Error");
        } 
    });
}

function generateTab(tabId) {
    // Generate header
    var header = "<li " + (tabId === 1 ? "class='active'" : "") + "><a href='#circuits-container' data-toggle='tab' onclick='getTab(" + tabId + ")'>C" + tabId + "</a></li>";

    // Append header
    $('#circuits-header').append(header);
}

function getTab(tabId) {
    // Generate content
    $.ajax({
        type: "GET",
        data: { id: tabId },
        contentType: "html",
        url: '/Circuits/Tab',
        success: function (data) {

            // Replace tab
            $('#circuits-container').html(data);

            // Reload events
            reloadFormsEvent();

            // Reload windings
            reloadWindings(tabId);
        },
        error: function (status) {
            alert("Error");
        }
    });
}