using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DTSWeb.Data;
using DTSWeb.Models;
using Microsoft.ApplicationInsights;
using Microsoft.AspNetCore.Identity;

namespace DTSWeb.Controllers
{
    public class TransformersController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        private Task<ApplicationUser> GetCurrentUserAsync() => _userManager.GetUserAsync(HttpContext.User);
        private readonly TelemetryClient _telemetryClient;

        public TransformersController(ApplicationDbContext context, TelemetryClient telemetryClient, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _telemetryClient = telemetryClient;
            _userManager = userManager;
        }

        // GET: Transformers
        public async Task<IActionResult> Index()
        {
            // Populate select lists
            ViewBag.Phases = new SelectList(_context.Phases, "PhaseId", "Value");
            ViewBag.CoolingTypes = new SelectList(_context.CoolingTypes, "CoolingTypeId", "Name");

            var transformer = await _context.Transformers.FindAsync(TransformerData.TransformerId);
     
            return PartialView("\\Views\\TransformerData\\_MainDataPartial.cshtml", transformer);
        }

        // GET: Transformers/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var transformer = await _context.Transformers
                .SingleOrDefaultAsync(m => m.TransformerId == id);
            if (transformer == null)
            {
                return NotFound();
            }

            return Ok(transformer);
        }

        // POST: Transformers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit([Bind("TransformerId,PhaseId,CoolingTypeId,DesignId,KVa,Frequency,Phases,Acr,Tamb,Tref")] Transformer transformer)
        { 

            if (ModelState.IsValid)
            {
                try
                {
                    // Get current user
                    var user = await GetCurrentUserAsync();

                    // If the transformer doesn't exist, create it; otherwise, update it
                    if (transformer.TransformerId == 0)
                    {          
                        _context.Add(transformer);
                        _telemetryClient.TrackTrace($"New transformer added by user with ID {user.Id}: Transformer {transformer.DesignId}");
                    }
                    else
                    {
                        _context.Update(transformer);
                        _telemetryClient.TrackTrace($"Transformer updated by user with ID {user.Id}: Transformer ID {transformer.TransformerId}");
                    }

                    await _context.SaveChangesAsync();
                }
                catch (Exception ex)
                {
                    _telemetryClient.TrackException(ex);
                } 
            }

            // Save TransformerId
            if (transformer.TransformerId != 0)
            {
                TransformerData.TransformerId = transformer.TransformerId;
            }

            return RedirectToAction("Index");
        }

        // GET: Transformers/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var transformer = await _context.Transformers
                .SingleOrDefaultAsync(m => m.TransformerId == id);
            if (transformer == null)
            {
                return NotFound();
            }

            return View(transformer);
        }

        // POST: Transformers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var transformer = await _context.Transformers.SingleOrDefaultAsync(m => m.TransformerId == id);
            _context.Transformers.Remove(transformer);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool TransformerExists(int id)
        {
            return _context.Transformers.Any(e => e.TransformerId == id);
        }
    }
}
