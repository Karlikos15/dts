﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DTSWeb.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

namespace DTSWeb.Controllers
{
    public class HomeController : Controller
    {
        private ApplicationDbContext _context;

        public HomeController(ApplicationDbContext context)
        {
            _context = context;
        }

        public IActionResult Index()
        {
            // Populate select lists
            ViewBag.Phases = new SelectList(_context.Phases, "PhaseId", "Value");
            ViewBag.CoolingTypes = new SelectList(_context.CoolingTypes, "CoolingTypeId", "Name");
            ViewBag.LegShapes = new SelectList(_context.LegShapes, "LegShapeId", "Name");
            ViewBag.WindingConnections = new SelectList(_context.WindingConnections, "WindingConnectionId", "Name");        

            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View();
        }
    }
}
