using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DTSWeb.Data;
using DTSWeb.Models;
using Microsoft.ApplicationInsights;
using Microsoft.AspNetCore.Identity;

namespace DTSWeb.Controllers
{
    public class CoresController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        private Task<ApplicationUser> GetCurrentUserAsync() => _userManager.GetUserAsync(HttpContext.User);
        private readonly TelemetryClient _telemetryClient;

        public CoresController(ApplicationDbContext context, TelemetryClient telemetryClient, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _telemetryClient = telemetryClient;
            _userManager = userManager;
        }

        // GET: Cores
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.Cores.Include(c => c.LegShape);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: Cores/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var core = await _context.Cores
                .Include(c => c.LegShape)
                .SingleOrDefaultAsync(m => m.CoreId == id);
            if (core == null)
            {
                return NotFound();
            }

            return View(core);
        }

        // GET: Cores/Create
        public IActionResult Create()
        {
            ViewData["LegShapeId"] = new SelectList(_context.Set<LegShape>(), "LegShapeId", "LegShapeId");
            return View();
        }

        // POST: Cores/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("CoreId,LegShapeId,Net,GrossCsa,MaxWidth,Diam,WindowWidth,LegCenters,VoltsPerTurn,B,Nll,CoolingDucts,Width")] Core core)
        {
            if (ModelState.IsValid)
            {
                _context.Add(core);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewData["LegShapeId"] = new SelectList(_context.Set<LegShape>(), "LegShapeId", "LegShapeId", core.LegShapeId);
            return View(core);
        }

        // GET: Cores/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var core = await _context.Cores.SingleOrDefaultAsync(m => m.CoreId == id);
            if (core == null)
            {
                return NotFound();
            }
            ViewData["LegShapeId"] = new SelectList(_context.Set<LegShape>(), "LegShapeId", "LegShapeId", core.LegShapeId);
            return View(core);
        }

        // POST: Cores/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("CoreId,LegShapeId,NetCsa,GrossCsa,MaxWidth,Diam,WindowWidth,LegCenters,VoltsPerTurn,B,Nll,CoolingDucts,Width")] Core core)
        {
            // If transformer doesn't exist, return 404
            if (TransformerData.TransformerId == null)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    // Get current user
                    var user = await GetCurrentUserAsync();

                    // Add TransformerId
                    core.TransformerId = TransformerData.TransformerId.Value;

                    // Check if a core for this transformer already exists in database
                    var coreExists = await _context.Cores.AnyAsync(c => c.TransformerId == TransformerData.TransformerId);

                    // If the core doesn't exist, create it; otherwise, update it
                    if (coreExists)
                    {
                        _context.Update(core);
                        _telemetryClient.TrackTrace($"Core updated by user with ID {user.Id}: Core ID {core.CoreId}");
                    }
                    else
                    {
                        _context.Add(core);
                        _telemetryClient.TrackTrace($"New core added by user with ID {user.Id}: Core ID {core.CoreId}");
                    }

                    await _context.SaveChangesAsync();
                }
                catch (Exception ex)
                {
                    _telemetryClient.TrackException(ex);
                }
            }

            return Ok(core.CoreId);
        }

        // GET: Cores/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var core = await _context.Cores
                .Include(c => c.LegShape)
                .SingleOrDefaultAsync(m => m.CoreId == id);
            if (core == null)
            {
                return NotFound();
            }

            return View(core);
        }

        // POST: Cores/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var core = await _context.Cores.SingleOrDefaultAsync(m => m.CoreId == id);
            _context.Cores.Remove(core);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool CoreExists(int id)
        {
            return _context.Cores.Any(e => e.CoreId == id);
        }
    }
}
