using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DTSWeb.Data;
using DTSWeb.Models;

namespace DTSWeb.Controllers
{
    public class WindingsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public WindingsController(ApplicationDbContext context)
        {
            _context = context;    
        }

        // GET: Windings/CircuitId
        [HttpGet]
        [Route("windings/{circuitId}")]
        public async Task<IActionResult> Read(int circuitId)
        {
            // Get circuit by TabPostion and TransformerId
            var circuit = await
                _context.Circuits.FirstOrDefaultAsync(c => c.TransformerId == TransformerData.TransformerId.Value && c.TabPosition == circuitId);

            if (circuit == null)
            {
                return NotFound();
            }


            // Get associated windings
            var windings = await _context.Windings.Where(w => w.CircuitId == circuit.CircuitId).ToListAsync();

            return Ok(windings);
        }

        // POST: Windings
        [HttpPost]
        [Route("windings")]
        public async Task<IActionResult> Create([Bind("TechnologyId,DesignId,Tap")] Winding winding, int tabPosition)
        {
            if (ModelState.IsValid)
            {
                // Get circuit by TabPostion and TransformerId
                var circuit = await
                    _context.Circuits.FirstOrDefaultAsync(c => c.TransformerId == TransformerData.TransformerId.Value && c.TabPosition == tabPosition);

                // Append to winding
                winding.CircuitId = circuit.CircuitId;

                _context.Add(winding);
                await _context.SaveChangesAsync();
                return Ok(winding);
            }

            return BadRequest();
        }

        // PUT: Windings
        [HttpPut]
        [Route("windings/{id}")]
        public async Task<IActionResult> Update(int id, [Bind("WindingId,CircuitId,TechnologyId,DesignId,Tap")] Winding winding)
        {
            if (id != winding.WindingId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(winding);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!WindingExists(winding.WindingId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return NoContent();
            }
            
            return BadRequest();
        }

        // DELETE: Windings
        [Route("windings/{id}")]
        [HttpDelete]
        public async Task<IActionResult> Delete(int id)
        {
            var winding = await _context.Windings.SingleOrDefaultAsync(m => m.WindingId == id);
            _context.Windings.Remove(winding);
            await _context.SaveChangesAsync();
            return Ok();
        }

        private bool WindingExists(int id)
        {
            return _context.Windings.Any(e => e.WindingId == id);
        }
    }
}
