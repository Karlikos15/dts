using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DTSWeb.Data;
using DTSWeb.Models;
using Microsoft.ApplicationInsights;
using Microsoft.AspNetCore.Identity;

namespace DTSWeb.Controllers
{
    public class CircuitsController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        private Task<ApplicationUser> GetCurrentUserAsync() => _userManager.GetUserAsync(HttpContext.User);
        private readonly TelemetryClient _telemetryClient;

        public CircuitsController(ApplicationDbContext context, TelemetryClient telemetryClient, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _telemetryClient = telemetryClient;
            _userManager = userManager;
        }

        // GET: Circuits/Tab
        public async Task<IActionResult> Tab(int id)
        {
            ViewBag.Sequences = new SelectList(_context.Sequences, "SequenceId", "Name");
            ViewBag.CircuitConnections = new SelectList(_context.CircuitConnections, "CircuitConnectionId", "Name");
 
            Transformer transformer = null;

            if (TransformerData.TransformerId != null && TransformerData.TransformerId != 0)
            {
                transformer = await _context.Transformers.FindAsync(TransformerData.TransformerId.Value);
            }

            // If there is no transformer yet, create one
            if (transformer == null)
            {
                transformer = new Transformer();

                // Save changes to generate id
                await _context.SaveChangesAsync();

                // Save id
                TransformerData.TransformerId = transformer.TransformerId;
            }

            var circuit = await
                _context.Circuits.FirstOrDefaultAsync(c => c.TransformerId == TransformerData.TransformerId.Value && c.TabPosition == id);


            if (circuit == null)
            {
                circuit = new Circuit {TabPosition = id, CircuitConnectionId = 1, CircuitConnection = await _context.CircuitConnections.FindAsync(1), TransformerId = transformer.TransformerId, SequenceId = 1};

                _context.Circuits.Add(circuit);

                try
                {
                    await _context.SaveChangesAsync();
                }
                catch (Exception e)
                {
                    Debug.WriteLine(e);
                }
            }

            // If it is the first circuit, its kVa is equal to Transformer KVa
            if (id == 1 && circuit.KVa != transformer.KVa)
            {
                circuit.KVa = transformer.KVa;

                await _context.SaveChangesAsync();  
            }

            return PartialView("~/Views/TransformerData/_CircuitsPartial.cshtml", circuit);
        }
        

        // GET: Circuits/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var circuit = await _context.Circuits
                .Include(c => c.CircuitConnection)
                .Include(c => c.Sequence)
                .SingleOrDefaultAsync(m => m.CircuitId == id);
            if (circuit == null)
            {
                return NotFound();
            }

            return View(circuit);
        }

        // GET: Circuits/Create
        public IActionResult Create()
        {
            ViewData["CircuitConnectionId"] = new SelectList(_context.CircuitConnections, "CircuitConnectionId", "CircuitConnectionId");
            ViewData["SequenceId"] = new SelectList(_context.Sequences, "Id", "Id");
            return View();
        }

        // POST: Circuits/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("CircuitId,CircuitConnectionId,SequenceId,KVa,Vline,MinTapping,MaxTapping,AnalyzeTapping")] Circuit circuit)
        {
            if (ModelState.IsValid)
            {
                _context.Add(circuit);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewData["CircuitConnectionId"] = new SelectList(_context.CircuitConnections, "CircuitConnectionId", "CircuitConnectionId", circuit.CircuitConnectionId);
            ViewData["SequenceId"] = new SelectList(_context.Sequences, "Id", "Id", circuit.SequenceId);
            return View(circuit);
        }

        // GET: Circuits/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var circuit = await _context.Circuits.SingleOrDefaultAsync(m => m.CircuitId == id);
            if (circuit == null)
            {
                return NotFound();
            }
            ViewData["CircuitConnectionId"] = new SelectList(_context.CircuitConnections, "CircuitConnectionId", "CircuitConnectionId", circuit.CircuitConnectionId);
            ViewData["SequenceId"] = new SelectList(_context.Sequences, "Id", "Id", circuit.SequenceId);
            return View(circuit);
        }

        // POST: Circuits/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("CircuitId,CircuitConnectionId,SequenceId,KVa,Vline,MinTapping,MaxTapping,AnalyzeTapping,TabPosition")] Circuit circuit)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    // Get current user
                    var user = await GetCurrentUserAsync();

                    // If transformer doesn't exist, return 404
                    if (TransformerData.TransformerId == null || TransformerData.TransformerId == 0)
                    {
                        return NotFound();
                    }

                    // Append TransformerId
                    circuit.TransformerId = TransformerData.TransformerId.Value;

                    // If the circuit doesn't exist
                    if (circuit.CircuitId == 0)
                    {
                        _context.Add(circuit);
                        _telemetryClient.TrackTrace($"New circuit added by user with ID {user.Id}: Circuit ID {circuit.CircuitId}");
                    }
                    else 
                    {
                        _context.Update(circuit);
                        _telemetryClient.TrackTrace($"Circuit updated by user with ID {user.Id}: Circuit ID {circuit.CircuitId}");
                    }

                    await _context.SaveChangesAsync();
                }
                catch (Exception ex)
                {
                    _telemetryClient.TrackException(ex);
                }
            }

            return RedirectToAction("Tab", new {id = circuit.TabPosition });
        }

        // GET: Circuits/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var circuit = await _context.Circuits
                .Include(c => c.CircuitConnection)
                .Include(c => c.Sequence)
                .SingleOrDefaultAsync(m => m.CircuitId == id);
            if (circuit == null)
            {
                return NotFound();
            }

            return View(circuit);
        }

        // POST: Circuits/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var circuit = await _context.Circuits.SingleOrDefaultAsync(m => m.CircuitId == id);
            _context.Circuits.Remove(circuit);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool CircuitExists(int id)
        {
            return _context.Circuits.Any(e => e.CircuitId == id);
        }
    }
}
