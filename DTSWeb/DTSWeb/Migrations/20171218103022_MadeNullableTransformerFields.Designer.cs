﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using DTSWeb.Data;

namespace DTSWeb.Migrations
{
    [DbContext(typeof(ApplicationDbContext))]
    [Migration("20171218103022_MadeNullableTransformerFields")]
    partial class MadeNullableTransformerFields
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.2")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("DTSWeb.Models.ApplicationUser", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("AccessFailedCount");

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<string>("Email")
                        .HasMaxLength(256);

                    b.Property<bool>("EmailConfirmed");

                    b.Property<bool>("LockoutEnabled");

                    b.Property<DateTimeOffset?>("LockoutEnd");

                    b.Property<string>("NormalizedEmail")
                        .HasMaxLength(256);

                    b.Property<string>("NormalizedUserName")
                        .HasMaxLength(256);

                    b.Property<string>("PasswordHash");

                    b.Property<string>("PhoneNumber");

                    b.Property<bool>("PhoneNumberConfirmed");

                    b.Property<string>("SecurityStamp");

                    b.Property<bool>("TwoFactorEnabled");

                    b.Property<string>("UserName")
                        .HasMaxLength(256);

                    b.HasKey("Id");

                    b.HasIndex("NormalizedEmail")
                        .HasName("EmailIndex");

                    b.HasIndex("NormalizedUserName")
                        .IsUnique()
                        .HasName("UserNameIndex");

                    b.ToTable("AspNetUsers");
                });

            modelBuilder.Entity("DTSWeb.Models.Circuit", b =>
                {
                    b.Property<int>("CircuitId")
                        .ValueGeneratedOnAdd();

                    b.Property<double>("AnalyzeTapping");

                    b.Property<int>("CircuitConnectionId");

                    b.Property<double>("KVa");

                    b.Property<double>("MaxTapping");

                    b.Property<double>("MinTapping");

                    b.Property<int>("SequenceId");

                    b.Property<int>("TabPosition");

                    b.Property<int>("TransformerId");

                    b.Property<double>("Vline");

                    b.HasKey("CircuitId");

                    b.HasIndex("CircuitConnectionId");

                    b.HasIndex("SequenceId");

                    b.HasIndex("TransformerId");

                    b.ToTable("Circuits");
                });

            modelBuilder.Entity("DTSWeb.Models.CircuitConnection", b =>
                {
                    b.Property<int>("CircuitConnectionId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.HasKey("CircuitConnectionId");

                    b.ToTable("CircuitConnections");
                });

            modelBuilder.Entity("DTSWeb.Models.CoolingDuct", b =>
                {
                    b.Property<int>("CoolingDuctId")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("WindingId");

                    b.HasKey("CoolingDuctId");

                    b.HasIndex("WindingId");

                    b.ToTable("CoolingDucts");
                });

            modelBuilder.Entity("DTSWeb.Models.CoolingType", b =>
                {
                    b.Property<int>("CoolingTypeId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.HasKey("CoolingTypeId");

                    b.ToTable("CoolingTypes");
                });

            modelBuilder.Entity("DTSWeb.Models.Core", b =>
                {
                    b.Property<int>("CoreId")
                        .ValueGeneratedOnAdd();

                    b.Property<double>("B");

                    b.Property<int>("CoolingDucts");

                    b.Property<double>("Diam");

                    b.Property<double>("GrossCsa");

                    b.Property<double>("LegCenters");

                    b.Property<int>("LegShapeId");

                    b.Property<double>("MaxWidth");

                    b.Property<double>("NetCsa");

                    b.Property<double>("Nll");

                    b.Property<int>("TransformerId");

                    b.Property<double>("VoltsPerTurn");

                    b.Property<double>("Width");

                    b.Property<double>("WindowWidth");

                    b.HasKey("CoreId");

                    b.HasIndex("LegShapeId");

                    b.HasIndex("TransformerId");

                    b.ToTable("Cores");
                });

            modelBuilder.Entity("DTSWeb.Models.Disk", b =>
                {
                    b.Property<int>("DiskId")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("WindingId");

                    b.HasKey("DiskId");

                    b.HasIndex("WindingId");

                    b.ToTable("Disks");
                });

            modelBuilder.Entity("DTSWeb.Models.Lead", b =>
                {
                    b.Property<int>("LeadId")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("WindingId");

                    b.HasKey("LeadId");

                    b.HasIndex("WindingId");

                    b.ToTable("Leads");
                });

            modelBuilder.Entity("DTSWeb.Models.LegShape", b =>
                {
                    b.Property<int>("LegShapeId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.HasKey("LegShapeId");

                    b.ToTable("LegShapes");
                });

            modelBuilder.Entity("DTSWeb.Models.MechanicalGroup", b =>
                {
                    b.Property<int>("MechanicalGroupId")
                        .ValueGeneratedOnAdd();

                    b.HasKey("MechanicalGroupId");

                    b.ToTable("MechanicalGroups");
                });

            modelBuilder.Entity("DTSWeb.Models.Phase", b =>
                {
                    b.Property<int>("PhaseId")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("Value");

                    b.HasKey("PhaseId");

                    b.ToTable("Phases");
                });

            modelBuilder.Entity("DTSWeb.Models.Result", b =>
                {
                    b.Property<int>("ResultId")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("TransformerId");

                    b.HasKey("ResultId");

                    b.HasIndex("TransformerId");

                    b.ToTable("Results");
                });

            modelBuilder.Entity("DTSWeb.Models.Sequence", b =>
                {
                    b.Property<int>("SequenceId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.HasKey("SequenceId");

                    b.ToTable("Sequences");
                });

            modelBuilder.Entity("DTSWeb.Models.Strap", b =>
                {
                    b.Property<int>("StrapId")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("TurnId");

                    b.HasKey("StrapId");

                    b.HasIndex("TurnId");

                    b.ToTable("Straps");
                });

            modelBuilder.Entity("DTSWeb.Models.Technology", b =>
                {
                    b.Property<int>("TechnologyId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.HasKey("TechnologyId");

                    b.ToTable("Technologies");
                });

            modelBuilder.Entity("DTSWeb.Models.Transformer", b =>
                {
                    b.Property<int>("TransformerId")
                        .ValueGeneratedOnAdd();

                    b.Property<bool?>("Acr");

                    b.Property<int?>("CoolingTypeId");

                    b.Property<string>("DesignId");

                    b.Property<double?>("Frequency");

                    b.Property<double?>("KVa");

                    b.Property<int?>("PhaseId");

                    b.Property<int?>("Tamb");

                    b.Property<int?>("Tref");

                    b.HasKey("TransformerId");

                    b.HasIndex("CoolingTypeId");

                    b.HasIndex("PhaseId");

                    b.ToTable("Transformers");
                });

            modelBuilder.Entity("DTSWeb.Models.Turn", b =>
                {
                    b.Property<int>("TurnId")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("WindingId");

                    b.HasKey("TurnId");

                    b.HasIndex("WindingId");

                    b.ToTable("Turns");
                });

            modelBuilder.Entity("DTSWeb.Models.Winding", b =>
                {
                    b.Property<int>("WindingId")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("CircuitId");

                    b.Property<string>("DesignId");

                    b.Property<int?>("MechanicalGroupId");

                    b.Property<double>("Tap");

                    b.Property<int>("TechnologyId");

                    b.HasKey("WindingId");

                    b.HasIndex("CircuitId");

                    b.HasIndex("MechanicalGroupId");

                    b.HasIndex("TechnologyId");

                    b.ToTable("Windings");
                });

            modelBuilder.Entity("DTSWeb.Models.WindingConnection", b =>
                {
                    b.Property<int>("WindingConnectionId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.HasKey("WindingConnectionId");

                    b.ToTable("WindingConnections");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRole", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<string>("Name")
                        .HasMaxLength(256);

                    b.Property<string>("NormalizedName")
                        .HasMaxLength(256);

                    b.HasKey("Id");

                    b.HasIndex("NormalizedName")
                        .IsUnique()
                        .HasName("RoleNameIndex");

                    b.ToTable("AspNetRoles");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRoleClaim<string>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<string>("RoleId")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("RoleId");

                    b.ToTable("AspNetRoleClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserClaim<string>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<string>("UserId")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserLogin<string>", b =>
                {
                    b.Property<string>("LoginProvider");

                    b.Property<string>("ProviderKey");

                    b.Property<string>("ProviderDisplayName");

                    b.Property<string>("UserId")
                        .IsRequired();

                    b.HasKey("LoginProvider", "ProviderKey");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserLogins");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserRole<string>", b =>
                {
                    b.Property<string>("UserId");

                    b.Property<string>("RoleId");

                    b.HasKey("UserId", "RoleId");

                    b.HasIndex("RoleId");

                    b.ToTable("AspNetUserRoles");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserToken<string>", b =>
                {
                    b.Property<string>("UserId");

                    b.Property<string>("LoginProvider");

                    b.Property<string>("Name");

                    b.Property<string>("Value");

                    b.HasKey("UserId", "LoginProvider", "Name");

                    b.ToTable("AspNetUserTokens");
                });

            modelBuilder.Entity("DTSWeb.Models.Circuit", b =>
                {
                    b.HasOne("DTSWeb.Models.CircuitConnection", "CircuitConnection")
                        .WithMany()
                        .HasForeignKey("CircuitConnectionId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("DTSWeb.Models.Sequence", "Sequence")
                        .WithMany()
                        .HasForeignKey("SequenceId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("DTSWeb.Models.Transformer", "Transformer")
                        .WithMany()
                        .HasForeignKey("TransformerId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("DTSWeb.Models.CoolingDuct", b =>
                {
                    b.HasOne("DTSWeb.Models.Winding", "Winding")
                        .WithMany()
                        .HasForeignKey("WindingId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("DTSWeb.Models.Core", b =>
                {
                    b.HasOne("DTSWeb.Models.LegShape", "LegShape")
                        .WithMany()
                        .HasForeignKey("LegShapeId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("DTSWeb.Models.Transformer", "Transformer")
                        .WithMany()
                        .HasForeignKey("TransformerId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("DTSWeb.Models.Disk", b =>
                {
                    b.HasOne("DTSWeb.Models.Winding", "Winding")
                        .WithMany()
                        .HasForeignKey("WindingId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("DTSWeb.Models.Lead", b =>
                {
                    b.HasOne("DTSWeb.Models.Winding", "Winding")
                        .WithMany()
                        .HasForeignKey("WindingId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("DTSWeb.Models.Result", b =>
                {
                    b.HasOne("DTSWeb.Models.Transformer", "Transformer")
                        .WithMany()
                        .HasForeignKey("TransformerId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("DTSWeb.Models.Strap", b =>
                {
                    b.HasOne("DTSWeb.Models.Turn", "Turn")
                        .WithMany()
                        .HasForeignKey("TurnId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("DTSWeb.Models.Transformer", b =>
                {
                    b.HasOne("DTSWeb.Models.CoolingType", "CoolingType")
                        .WithMany()
                        .HasForeignKey("CoolingTypeId");

                    b.HasOne("DTSWeb.Models.Phase", "Phase")
                        .WithMany()
                        .HasForeignKey("PhaseId");
                });

            modelBuilder.Entity("DTSWeb.Models.Turn", b =>
                {
                    b.HasOne("DTSWeb.Models.Winding", "Winding")
                        .WithMany()
                        .HasForeignKey("WindingId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("DTSWeb.Models.Winding", b =>
                {
                    b.HasOne("DTSWeb.Models.Circuit", "Circuit")
                        .WithMany()
                        .HasForeignKey("CircuitId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("DTSWeb.Models.MechanicalGroup", "MechanicalGroup")
                        .WithMany()
                        .HasForeignKey("MechanicalGroupId");

                    b.HasOne("DTSWeb.Models.Technology", "Technology")
                        .WithMany()
                        .HasForeignKey("TechnologyId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRoleClaim<string>", b =>
                {
                    b.HasOne("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRole")
                        .WithMany("Claims")
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserClaim<string>", b =>
                {
                    b.HasOne("DTSWeb.Models.ApplicationUser")
                        .WithMany("Claims")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserLogin<string>", b =>
                {
                    b.HasOne("DTSWeb.Models.ApplicationUser")
                        .WithMany("Logins")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserRole<string>", b =>
                {
                    b.HasOne("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRole")
                        .WithMany("Users")
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("DTSWeb.Models.ApplicationUser")
                        .WithMany("Roles")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
        }
    }
}
