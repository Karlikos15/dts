﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DTSWeb.Migrations
{
    public partial class MadeNullableTransformerFields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Transformers_CoolingTypes_CoolingTypeId",
                table: "Transformers");

            migrationBuilder.DropForeignKey(
                name: "FK_Transformers_Phases_PhaseId",
                table: "Transformers");

            migrationBuilder.AlterColumn<int>(
                name: "Tref",
                table: "Transformers",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "Tamb",
                table: "Transformers",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "PhaseId",
                table: "Transformers",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<double>(
                name: "KVa",
                table: "Transformers",
                nullable: true,
                oldClrType: typeof(double));

            migrationBuilder.AlterColumn<double>(
                name: "Frequency",
                table: "Transformers",
                nullable: true,
                oldClrType: typeof(double));

            migrationBuilder.AlterColumn<int>(
                name: "CoolingTypeId",
                table: "Transformers",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<bool>(
                name: "Acr",
                table: "Transformers",
                nullable: true,
                oldClrType: typeof(bool));

            migrationBuilder.AddForeignKey(
                name: "FK_Transformers_CoolingTypes_CoolingTypeId",
                table: "Transformers",
                column: "CoolingTypeId",
                principalTable: "CoolingTypes",
                principalColumn: "CoolingTypeId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Transformers_Phases_PhaseId",
                table: "Transformers",
                column: "PhaseId",
                principalTable: "Phases",
                principalColumn: "PhaseId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Transformers_CoolingTypes_CoolingTypeId",
                table: "Transformers");

            migrationBuilder.DropForeignKey(
                name: "FK_Transformers_Phases_PhaseId",
                table: "Transformers");

            migrationBuilder.AlterColumn<int>(
                name: "Tref",
                table: "Transformers",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "Tamb",
                table: "Transformers",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "PhaseId",
                table: "Transformers",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<double>(
                name: "KVa",
                table: "Transformers",
                nullable: false,
                oldClrType: typeof(double),
                oldNullable: true);

            migrationBuilder.AlterColumn<double>(
                name: "Frequency",
                table: "Transformers",
                nullable: false,
                oldClrType: typeof(double),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "CoolingTypeId",
                table: "Transformers",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<bool>(
                name: "Acr",
                table: "Transformers",
                nullable: false,
                oldClrType: typeof(bool),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Transformers_CoolingTypes_CoolingTypeId",
                table: "Transformers",
                column: "CoolingTypeId",
                principalTable: "CoolingTypes",
                principalColumn: "CoolingTypeId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Transformers_Phases_PhaseId",
                table: "Transformers",
                column: "PhaseId",
                principalTable: "Phases",
                principalColumn: "PhaseId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
