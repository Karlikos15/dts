﻿using System.Threading;
using System.Threading.Tasks;
using DTSWeb.Data;
using DTSWeb.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace DTSWeb.Auth
{
    public class UserLdapStore : UserStore<ApplicationUser>
    {

        public UserLdapStore(ApplicationDbContext context, IdentityErrorDescriber describer = null)
            : base(context, describer) {
        }
        public async Task<string> GetDistinguishedNameAsync(ApplicationUser user)
        {
            var dn = $"UID={user.UserName},DC=example,DC=com";

            return dn;
        }
    }
}