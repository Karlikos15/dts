﻿/******************  P5.JS DRAWING  ******************/
function setup() {
    /* Canvas specs */

    // Size
    var canvasWidth = windowWidth*320/1366;
    var canvasHeight = canvasWidth;

    /************************/

    /* Main container specs */

    // Origin
    var mainContainerX = canvasWidth/14;
    var mainContainerY = 25;

    // Size
    var mainContainerWidth = 360;
    var mainContainerHeight = 360;

    // Round corners
    var mainContainerBorderRadius = 5;

    /************************/

    /* Core specs */

    // Origin
    var coreX = 30 + mainContainerX;
    var coreY = 30 + mainContainerY;

    // Size
    var coreWidth = 50;
    var coreHeight = mainContainerHeight -60;

    // Round corners
    var coreBorderRadius = 5;

    /**************/

    /* Mech group 1 specs */

    // Origin
    var mechGroup1X = 20 + coreX + coreWidth;
    var mechGroup1Y = coreY;

    // Size
    var mechGroup1Width = 240;
    var mechGroup1Height = 140;

    // Round corners
    var mechGroup1BorderRadius = 5;

    /**************/

    /* Mech group 2 specs */

    // Origin
    var mechGroup2X = 20 + coreX + coreWidth;
    var mechGroup2Y = mechGroup1Y + mechGroup1Height + 20;

    // Size
    var mechGroup2Width = 240;
    var mechGroup2Height = 140;

    // Round corners
    var mechGroup2BorderRadius = 5;

    /**************/

    /**************/

    /* Winding1 specs */

    // Origin
    var winding1X = 10 + mechGroup1X;
    var winding1Y = 10 + mechGroup1Y;

    // Size
    var winding1Width = mechGroup1Width/2-10;
    var winding1Height = mechGroup1Height/2;

    // Round corners
    var winding1BorderRadius = 5;

    /**************/

    /* Winding2 specs */

    // Origin
    var winding2X = 10 + winding1X + winding1Width;
    var winding2Y = winding1Y + winding1Height/2;    

    // Size
    var winding2Width = mechGroup1Width/2-20;
    var winding2Height = mechGroup1Height/2;

    // Round corners
    var winding2BorderRadius = 5;

    /**************/

    /* Winding3 specs */

    // Origin
    var winding3X = 10 + mechGroup2X;
    var winding3Y = 10 + mechGroup2Y;

    // Size
    var winding3Width = mechGroup2Width - 20;
    var winding3Height = mechGroup2Height -20;

    // Round corners
    var winding3BorderRadius = 5;

    /**************/

    // Create the canvas
    var canvas = createCanvas(canvasWidth, canvasHeight);

    // Position the canvas into <div id="transformer-structure">
    canvas.parent('transformer-structure-container');

    // Set background color
    background('#C6C5C5');

    // Set colors (red)
    fill(255, 0, 0, 100);
    stroke(150, 0, 0);

    // Draw main container
    rect(mainContainerX, mainContainerY, mainContainerWidth, mainContainerHeight, mainContainerBorderRadius);

    // Set colors (red)
    fill(255, 0, 0, 140);
    stroke(150, 0, 0);

    // Draw core
    rect(coreX, coreY, coreWidth, coreHeight, coreBorderRadius);

    /**
     * TOP MECH GROUP
     */

    // Set colors (orange)
    fill(255, 50, 0, 120);
    stroke(245, 40, 0);

    // Draw mechanical group
    rect(mechGroup1X, mechGroup1Y, mechGroup1Width, mechGroup1Height, mechGroup1BorderRadius);

    // Set colors (green)
    fill(0, 200, 0, 140);
    stroke(0, 150, 0);

    // Draw winding1
    rect(winding1X, winding1Y, winding1Width, winding1Height, winding1BorderRadius);

    // Draw winding2
    rect(winding2X, winding2Y, winding2Width, winding2Height, winding2BorderRadius);

    /**
     * BOTTOM MECH GROUP
     */

    // Set colors (orange)
    fill(255, 50, 0, 120);
    stroke(245, 40, 0);

    // Draw mechanical group
    rect(mechGroup2X, mechGroup2Y, mechGroup2Width, mechGroup2Height, mechGroup2BorderRadius);

    // Set colors (green)
    fill(0, 200, 0, 140);
    stroke(0, 150, 0);

    // Draw winding3
    rect(winding3X, winding3Y, winding3Width, winding3Height, winding3BorderRadius);


}