﻿var windings = [
    { "ID": "HV1", "Technology": 1, "Tap": 40 },
    { "ID": "HV2", "Technology": 1, "Tap": 40 },
    { "ID": "HV3", "Technology": 1, "Tap": 40 },
    { "ID": "HV4", "Technology": 1, "Tap": 40 },
    { "ID": "HV5", "Technology": 1, "Tap": 40 },
    { "ID": "HV6", "Technology": 1, "Tap": 40 }
];

var technologies = [
    { Name: "Disk-Foil", Id: 0 },
    { Name: "Disk-Foil", Id: 1 },
    { Name: "Disk-Foil", Id: 2 },
    { Name: "Disk-Foil", Id: 3 }
];


function initializeDataGrid() {
    $(".windings-circuit-grid").jsGrid({
        width: "100%",

        inserting: true,
        editing: true,
        sorting: true,
        //paging: true,

        invalidMessage: "Invalid data entered",

        editButton: false,

        data: windings,

        fields: [
            {
                name: "ID", type: "text", validate: "required",
                editTemplate: function (value, item) {
                    var $input = jsGrid.fields.text.prototype.editTemplate.apply(this, arguments);
                    return $input.attr('class', 'form-control').attr('value', value);
                }
            },

            {
                name: "Technology", type: "select", items: technologies, valueField: "Id", textField: "Name",
                editTemplate: function (value, item) {
                    var $select = jsGrid.fields.select.prototype.editTemplate.apply(this, arguments);
                    return $select.attr('class', 'form-control');
                }
            },

            {
                name: "Tap [%]", type: "number",
                editTemplate: function (value, item) {
                    var $input = jsGrid.fields.text.prototype.editTemplate.apply(this, arguments);
                    return $input.attr('class', 'form-control').attr('value', value);
                }
            },

            // Generic template
            {
                type: "control",
                itemTemplate: function (value, item) {
                    var $result = $([]);

                    var $deleteButton = $(this._createDeleteButton(item));

                    //$deleteButton.css("background-image", "none").val("&#xf1f8;");

                    // Just add edit icon, not delete
                    $result = $result.add($deleteButton);

                    return $result;
                }
            }
        ]
    });
}



$(function () {
    initializeDataGrid();
});
