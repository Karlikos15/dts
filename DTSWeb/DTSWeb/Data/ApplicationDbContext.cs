﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using DTSWeb.Models;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace DTSWeb.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);
        }

        public DbSet<Circuit> Circuits { get; set; }
        public DbSet<CoolingDuct> CoolingDucts { get; set; }
        public DbSet<Core> Cores { get; set; }
        public DbSet<Disk> Disks { get; set; }
        public DbSet<Lead> Leads { get; set; }
        public DbSet<MechanicalGroup> MechanicalGroups { get; set; }
        public DbSet<Result> Results { get; set; }
        public ShieldingScreen ShieldingScreens { get; set; }
        public DbSet<Strap> Straps { get; set; }
        public DbSet<Technology> Technologies { get; set; }
        public DbSet<Transformer> Transformers { get; set; }
        public DbSet<Turn> Turns { get; set; }
        public DbSet<Winding> Windings { get; set; }
        public DbSet<Phase> Phases { get; set; }
        public DbSet<CoolingType> CoolingTypes { get; set; }
        public DbSet<LegShape> LegShapes { get; set; }
        public DbSet<CircuitConnection> CircuitConnections { get; set; }
        public DbSet<WindingConnection> WindingConnections { get; set; }
        public DbSet<Sequence> Sequences { get; set; }
    }
}
